import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  
  if (fighter) {
    const imgElement = createFighterImage(fighter);
    const infoElement = createFighterInfo(fighter);
    fighterElement.appendChild(imgElement);
    fighterElement.appendChild(infoElement);
  };
  
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterInfo(fighter) {
  const { name, health, attack, defense } = fighter;
  const pElement = document.createElement('p');
  pElement.classList.add("bag")
  const pText = document.createTextNode(`Name: ${name} Health: ${health} Attack: ${attack} Defence: ${defense}`);
  pElement.appendChild(pText);
  
  return pElement;
}