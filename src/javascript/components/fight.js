import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const arenaFirstFighter = arenaFighter(firstFighter);
    const arenaSecondFighter = arenaFighter(secondFighter);
    
    const pressedKeys = new Map();

    document.addEventListener('keydown', (e) => {
      pressedKeys.set(e.code, true);
      
      actionKey(arenaFirstFighter, arenaSecondFighter, pressedKeys);

      if (arenaFirstFighter.currentHealth <= 0 || arenaSecondFighter.currentHealth <= 0) {
        const winner = arenaFirstFighter.currentHealth <= 0 ? secondFighter : firstFighter;
        resolve(winner);
      };
    });
    
    document.addEventListener('keyup', (e) => {
      pressedKeys.delete(e.code);
    });
  });
}

function arenaFighter(fighter) {
  return {
    ...fighter,
    currentHealth: fighter.health,
    criticalHitCooldown: new Date(0),
    setCooldown() {
      this.criticalHitCooldown = new Date();
    },
  };
}

function actionKey(firstFighter, secondFighter, keyMap) {
  const leftHealthIndicator = document.getElementById('left-fighter-indicator');
  const rightHealthIndicator = document.getElementById('right-fighter-indicator');
  
  switch(true) {
    case keyMap.has(controls.PlayerOneAttack): {
      atack(firstFighter, secondFighter, rightHealthIndicator, keyMap);
    };break;
    case keyMap.has(controls.PlayerTwoAttack): {
      atack(secondFighter, firstFighter, leftHealthIndicator, keyMap);
    };break;
    case controls.PlayerOneCriticalHitCombination.every(code => keyMap.has(code)): {
      atackCombination(firstFighter, secondFighter, rightHealthIndicator);
    };break;
    case controls.PlayerTwoCriticalHitCombination.every(code => keyMap.has(code)): {
      atackCombination(secondFighter, firstFighter, leftHealthIndicator);
    };break;
  };
}

function atack(attacker, defender, healthIndicator, keyMap) {
  if (isAttackBlocked(keyMap)) return; 
      
  defender.currentHealth -= getDamage(attacker, defender);
  updateHealthIndicator(defender, healthIndicator);
}

function atackCombination(attacker, defender, healthIndicator) {
  if (isCriticalHitCooldown(attacker)) return;

  defender.currentHealth -= attacker.attack * 2;
  updateHealthIndicator(defender, healthIndicator);
  
  attacker.setCooldown();
}

function isAttackBlocked(keyMap) {
  return keyMap.has(controls.PlayerOneBlock) || keyMap.has(controls.PlayerTwoBlock);
}

function isCriticalHitCooldown(attacker) {
  const cooldownSeconds = (new Date().getTime() - attacker.criticalHitCooldown.getTime()) / 1000;
  console.log(cooldownSeconds);
  return cooldownSeconds < 10;
} 

export function getDamage(attacker, defender) {
  return Math.max(0, getHitPower(attacker) - getBlockPower(defender));
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  const criticalHitChance = randomInteger(1, 2);
  
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = randomInteger(1, 2);
  
  return defense * dodgeChance; 
}

function updateHealthIndicator(defender, indicator) {
  const { health, currentHealth } = defender;

  const indicatorWidth = Math.max(0, (currentHealth * 100) / health);
  indicator.style.width = `${indicatorWidth}%`;
}

function randomInteger(min, max) {
  return Math.floor(min + Math.random() * (max + 1 - min));
}